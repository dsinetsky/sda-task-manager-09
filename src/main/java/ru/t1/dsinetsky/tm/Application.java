package ru.t1.dsinetsky.tm;

import ru.t1.dsinetsky.tm.api.ICommandController;
import ru.t1.dsinetsky.tm.api.ICommandRepository;
import ru.t1.dsinetsky.tm.api.ICommandService;
import ru.t1.dsinetsky.tm.component.Bootstrap;
import ru.t1.dsinetsky.tm.constant.ArgumentConst;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.controller.CommandController;
import ru.t1.dsinetsky.tm.repository.CommandRepository;
import ru.t1.dsinetsky.tm.service.CommandService;

import java.util.Scanner;

public class Application {



    public static void main(final String[] args) {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}

