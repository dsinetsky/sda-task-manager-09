package ru.t1.dsinetsky.tm.repository;

import ru.t1.dsinetsky.tm.api.ICommandRepository;
import ru.t1.dsinetsky.tm.constant.ArgumentConst;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    public static Command VERSION = new Command(
            TerminalConst.CMD_VERSION,
            ArgumentConst.CMD_VERSION,
            "Shows program version"
    );

    public static Command INFO = new Command(
            TerminalConst.CMD_INFO,
            ArgumentConst.CMD_INFO,
            "Shows system info"
    );

    public static Command ABOUT = new Command(
            TerminalConst.CMD_ABOUT,
            ArgumentConst.CMD_ABOUT,
            "Shows information about developer"

    );

    public static Command HELP = new Command(
            TerminalConst.CMD_HELP,
            ArgumentConst.CMD_HELP,
            "Shows this message"
    );

    public static Command EXIT = new Command(
            TerminalConst.CMD_EXIT,
            null,
            "Exit application"
    );

    public static Command ARG = new Command(
            TerminalConst.CMD_ARG,
            ArgumentConst.CMD_ARG,
            "Shows list of arguments"
    );

    public static Command CMD = new Command(
            TerminalConst.CMD_CMD,
            ArgumentConst.CMD_CMD,
            "Shows list of commands"
    );

    private static Command[] TERMINAL_COMMANDS = new Command[]{
            HELP, VERSION, ABOUT, INFO, ARG, CMD, EXIT
    };

    @Override
    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }
}
