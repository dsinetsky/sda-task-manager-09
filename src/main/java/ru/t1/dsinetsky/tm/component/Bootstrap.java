package ru.t1.dsinetsky.tm.component;

import ru.t1.dsinetsky.tm.api.ICommandController;
import ru.t1.dsinetsky.tm.api.ICommandRepository;
import ru.t1.dsinetsky.tm.api.ICommandService;
import ru.t1.dsinetsky.tm.constant.ArgumentConst;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.controller.CommandController;
import ru.t1.dsinetsky.tm.repository.CommandRepository;
import ru.t1.dsinetsky.tm.service.CommandService;

import java.util.Scanner;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();
    private final ICommandService commandService = new CommandService(commandRepository);
    private final ICommandController commandController = new CommandController(commandService);

    private void terminalRun(final String command) {
        if (command == null) {
            commandController.displayError();
            return;
        }
        switch (command) {
            case (TerminalConst.CMD_HELP):
                commandController.displayHelp();
                break;
            case (TerminalConst.CMD_VERSION):
                commandController.displayVersion();
                break;
            case (TerminalConst.CMD_ABOUT):
                commandController.displayAbout();
                break;
            case (TerminalConst.CMD_INFO):
                commandController.displayInfo();
                break;
            case (TerminalConst.CMD_ARG):
                commandController.displayArguments();
                break;
            case (TerminalConst.CMD_CMD):
                commandController.displayCommands();
                break;
            case (TerminalConst.CMD_EXIT):
                exitApp();
                break;
            default:
                commandController.displayError();
                break;
        }
    }

    private void argumentRun(final String arg) {
        switch (arg) {
            case (ArgumentConst.CMD_HELP):
                commandController.displayHelp();
                break;
            case (ArgumentConst.CMD_VERSION):
                commandController.displayVersion();
                break;
            case (ArgumentConst.CMD_ABOUT):
                commandController.displayAbout();
                break;
            case (ArgumentConst.CMD_INFO):
                commandController.displayInfo();
                break;
            case (ArgumentConst.CMD_CMD):
                commandController.displayCommands();
                break;
            case (ArgumentConst.CMD_ARG):
                commandController.displayArguments();
                break;
            default:
                commandController.displayError();
                break;
        }
    }

    private boolean argRun(final String[] args) {
        if (args.length < 1) {
            return false;
        }
        String param = args[0];
        argumentRun(param);
        return true;
    }


    private void exitApp() {
        System.exit(0);
    }

    public void run(final String[] args) {
        if (argRun(args)) {
            exitApp();
            return;
        }
        commandController.displayWelcome();
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("\nEnter command:");
            String command = scanner.nextLine();
            terminalRun(command);
        }
    }

}
